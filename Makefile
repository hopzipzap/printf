NAME = libftprintf.a

CC = gcc
FLAGS = -Wall -Werror -Wextra -O3
INCLUDES = -I$(HEADERS_DIRECTORY) -I$(LIBFT_HEADERS_DIRECTORY)

# LIBFT

LIBFT = $(LIBFT_DIRECTORY)libft.a
LIBFT_DIRECTORY = ./libft/

# LIBFT_HEADERS

LIBFT_HEADERS_LIST = \
	libft.h
LIBFT_HEADERS_DIRECTORY = $(LIBFT_DIRECTORY)includes/
LIBFT_HEADERS = $(addprefix $(LIBFT_HEADERS_DIRECTORY), $(LIBFT_HEADERS_LIST))

# LIBFT_SOURCES

LIBFT_SOURCES_DIRECTORY = $(LIBFT_DIRECTORY)sources/
LIBFT_SOURCES_LIST = \
			ft_itoa.c \
			ft_atoi.c  \
			ft_strcpy.c \
			ft_strdup.c  \
			ft_strlen.c   \
			ft_strnew.c    \
			ft_strdel.c     \
			ft_memcmp.c      \
			ft_memdel.c       \
			ft_memset.c        \
			ft_putstr.c         \
			ft_putnbr.c          \
			ft_putchar.c          \
			ft_strncpy.c           \
			ft_strjoin.c            \

LIBFT_SOURCES = $(addprefix $(LIBFT_SOURCES_DIRECTORY), $(LIBFT_SOURCES_LIST))

# LIBFT_OBJECTS

LIBFT_OBJECTS_DIRECTORY = $(LIBFT_DIRECTORY)objects/
LIBFT_OBJECTS_LIST = $(patsubst %.c, %.o, $(LIBFT_SOURCES_LIST))
LIBFT_OBJECTS = $(addprefix $(LIBFT_OBJECTS_DIRECTORY), $(LIBFT_OBJECTS_LIST))

# HEADERS

HEADERS_LIST = \
	ft_printf.h\
	
HEADERS_DIRECTORY = ./includes/
HEADERS = $(addprefix $(HEADERS_DIRECTORY), $(HEADERS_LIST))

# SOURCES

SOURCES_DIRECTORY = ./sources/
SOURCES_LIST = \
		check.c  \
		csp.c	\
		ft_printf.c  \
		diouxb.c  \
		scan_params.c   \
		fw.c  \
		length.c   \
		print_spec_unicode.c   \
		util_1.c   \
		print_unicode_util.c  \
		util_2.c   \
		util_atoi_translate.c \

SOURCES = $(addprefix $(SOURCES_DIRECTORY), $(SOURCES_LIST))

# OBJECTS

OBJECTS_DIRECTORY = objects/
OBJECTS_LIST = $(patsubst %.c, %.o, $(SOURCES_LIST))
OBJECTS	= $(addprefix $(OBJECTS_DIRECTORY), $(OBJECTS_LIST))

# COLORS

GREEN = \033[0;32m
RED = \033[0;31m
RESET = \033[0m

.PHONY: all clean fclean re

all: $(NAME)

$(NAME): $(LIBFT) $(LIBFT_OBJECTS) $(OBJECTS_DIRECTORY) $(OBJECTS)
	@ar rc $(NAME) $(OBJECTS) $(LIBFT_OBJECTS)
	@ranlib $(NAME)
	@echo "\n$(NAME): $(GREEN)object files were created$(RESET)"
	@echo "$(NAME): $(GREEN)$(NAME) was created$(RESET)"

$(OBJECTS_DIRECTORY):
	@mkdir -p $(OBJECTS_DIRECTORY)
	@echo "$(NAME): $(GREEN)$(OBJECTS_DIRECTORY) was created$(RESET)"

$(OBJECTS_DIRECTORY)%.o : $(SOURCES_DIRECTORY)%.c $(HEADERS)
	@$(CC) $(FLAGS) -c $(INCLUDES) $< -o $@
	@echo "$(GREEN).$(RESET)\c"

$(LIBFT):
	@echo "$(NAME): $(GREEN)Creating $(LIBFT)...$(RESET)"
	@$(MAKE) -sC $(LIBFT_DIRECTORY)

clean:
	@$(MAKE) -sC $(LIBFT_DIRECTORY) clean
	@rm -rf $(OBJECTS_DIRECTORY)
	@echo "$(NAME): $(RED)$(OBJECTS_DIRECTORY) was deleted$(RESET)"
	@echo "$(NAME): $(RED)object files were deleted$(RESET)"

fclean: clean
	@rm -f $(LIBFT)
	@echo "$(NAME): $(RED)$(LIBFT) was deleted$(RESET)"
	@rm -f $(NAME)
	@echo "$(NAME): $(RED)$(NAME) was deleted$(RESET)"

re:
	@$(MAKE) fclean
	@$(MAKE) all