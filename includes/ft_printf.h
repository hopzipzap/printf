/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstroman <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 19:09:16 by tstroman          #+#    #+#             */
/*   Updated: 2019/10/23 19:14:09 by tstroman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>
# include <stdio.h>
# include "../libft/includes/libft.h"

# define EOC	 "\033[0m"
# define RED     "\033[1;31m"
# define YELLOW  "\033[1;33m"
# define WHITE   "\033[1;37m"
# define BLACK	 "\033[0;30m"
# define GREEN	 "\033[0;32m"
# define BLUE	 "\033[0;34m"
# define PURPUL	 "\033[0;35m"
# define CYAN	 "\033[0;36m"

# define IS_NAN(num) ((num != num) ? 1 : 0)
# define IS_INF(num) ((num == (1.0 / 0.0) || num == (-1.0 / 0.0)) ? 1 : 0)

typedef struct		s_specifier
{
	int				prec;
	int				flag_minus;
	int				flag_plus;
	int				flag_space;
	int				flag_hash;
	int				flag_zero;
	int				length;
	int				width;
	char			spec;
}					t_spec;

int					is_correct(char c);
int					is_spec(char c);
int					is_flag(char c);
void				print_spec_p(t_spec spec, va_list ap, int *len);
void				print_s(t_spec spec, va_list ap, int *len);
void				print_c(t_spec spec, va_list ap, int *len);
void				print_spec_c(t_spec spec, va_list ap, int *len);
void				print_spec_s(t_spec spec, va_list ap, int *len);
void				print_spec_d_i(t_spec spec, long long int num, int *len);
void				print_spec_o(t_spec spec, unsigned long long num, int *len);
void				print_spec_u(t_spec spec, unsigned long long num, int *len);
void				print_spec_x(t_spec spec, unsigned long long num, int *len);
void				print_spec_b(t_spec spec, unsigned long long num, int *len);
void				scan_flag(t_spec *spec, char *format);
int					scan_prec(char *format, va_list ap);
int					scan_width(char *format, va_list ap);
int					scan_length(char *format);
int					if_present(const char *str, const char *sub);
int					scan_color(char *format);
void				perform_spec(t_spec spec, va_list ap, int *len);
int					scan_spec(char *format, va_list ap, int *len);
int					ft_printf(char *format, ...);
void				print_spec_f(t_spec spec, long double num, int *len);
long long int		length_d_i(t_spec spec, va_list ap);
long long int		length_u_o_x(t_spec spec, va_list ap);
long double			length_f(t_spec spec, va_list ap);
wchar_t				*unicode_prec(wchar_t *str, int prec);
void				print_spec_l_c(t_spec spec, va_list ap, int *len);
void				print_spec_l_s(t_spec spec, va_list ap, int *len);
void				print_unicode(wchar_t c, int *len);
void				print_unicode_str(wchar_t *str, int *len);
int					unicode_simblen(wchar_t c);
int					unicode_strlen(wchar_t *str);
unsigned long long	atoi_util(int num, int i);
void				alignment(t_spec spec, char *str, int *len);
char				*round_to(char *str);
int					ft_nbrlen(long long int num);
int					ft_nbrlen_u(unsigned long long num);
void				set_pref(char **str, char spec);
void				set_plus(char **str, t_spec spec);
void				add_zero(char **str, t_spec spec);
void				prec_zero(char **str, int prec);
int					ft_cat_pro(char **dest, char *src);
char				*ft_itoa_ll(long double n);
char				*ft_itoa_ull(unsigned long long nb);
char				*ft_itoa_base(int dec, int base, int up);
char				*ft_itoa_base_ull(unsigned long long dec,
															int base, char x);
char				*ft_ftoa(long double n, int precision);

#endif
