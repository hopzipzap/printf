/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fw.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstroman <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 18:18:04 by tstroman          #+#    #+#             */
/*   Updated: 2019/10/24 16:20:36 by tstroman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void				print_spec_f(t_spec spec, long double num, int *len)
{
	char			*str;

	if (spec.prec == -1)
		str = ft_ftoa(num, 0);
	else
		str = ft_ftoa(num, (!spec.prec) ? 6 : spec.prec);
	if (!if_present(str, "nan") && !if_present(str, "inf"))
	{
		prec_zero(&str, spec.prec);
		if (!spec.flag_hash && str[ft_strlen(str) - 1] == '.')
			str[ft_strlen(str) - 1] = '\0';
		if (spec.flag_plus || spec.flag_space)
			set_plus(&str, spec);
		if (spec.flag_zero && !spec.flag_minus)
			if ((int)ft_strlen(str) < spec.width)
				add_zero(&str, spec);
	}
	alignment(spec, str, len);
}
