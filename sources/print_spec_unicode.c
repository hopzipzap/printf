/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_spec_unicode.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstroman <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 18:21:52 by tstroman          #+#    #+#             */
/*   Updated: 2019/10/23 18:22:16 by tstroman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

wchar_t				*unicode_prec(wchar_t *str, int prec)
{
	int				i;
	int				len1;

	i = 0;
	len1 = 0;
	if (prec == -1)
		str[0] = '\0';
	if (prec > 0)
		while (str[i])
		{
			len1 += unicode_simblen(str[i]);
			if (len1 > prec)
				str[i] = '\0';
			i++;
		}
	return (str);
}

void				print_spec_l_c(t_spec spec, va_list ap, int *len)
{
	wchar_t			c;
	int				len1;

	c = va_arg(ap, wchar_t);
	len1 = unicode_simblen(c);
	if (!spec.flag_minus && spec.width)
		while (len1++ < spec.width)
			ft_putchar(' ', len);
	print_unicode(c, len);
	if (spec.flag_minus)
		while (len1++ < spec.width)
			ft_putchar(' ', len);
}

void				print_spec_l_s(t_spec spec, va_list ap, int *len)
{
	wchar_t			*str;
	int				len1;

	len1 = 0;
	if (!(str = va_arg(ap, wchar_t*)))
		str = unicode_strdup(L"(null)");
	str = unicode_prec(unicode_strdup(str), spec.prec);
	len1 = unicode_strlen(str);
	if (!spec.flag_minus)
		while (len1++ < spec.width)
			ft_putchar(' ', len);
	print_unicode_str(str, len);
	if (spec.flag_minus)
		while (len1++ < spec.width)
			ft_putchar(' ', len);
}
