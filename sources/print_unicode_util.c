/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_unicode_util.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstroman <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 18:22:45 by tstroman          #+#    #+#             */
/*   Updated: 2019/10/23 18:22:49 by tstroman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void				print_unicode(wchar_t c, int *len)
{
	if (c < 0x80)
		ft_putchar(c, len);
	else if (c < 0x800)
	{
		ft_putchar(c >> 6 | 0xC0, len);
		ft_putchar((c & 0x3F) | 0x80, len);
	}
	else if (c < 0x10000)
	{
		ft_putchar(c >> 12 | 0xE0, len);
		ft_putchar(((c >> 6) & 0x3F) | 0x80, len);
		ft_putchar((c & 0x3F) | 0x80, len);
	}
	else if (c < 0x110000)
	{
		ft_putchar(c >> 18 | 0xF0, len);
		ft_putchar(((c >> 12) & 0x3F) | 0x80, len);
		ft_putchar(((c >> 6) & 0x3F) | 0x80, len);
		ft_putchar((c & 0x3F) | 0x80, len);
	}
}

void				print_unicode_str(wchar_t *str, int *len)
{
	int				i;

	i = 0;
	if (str)
	{
		while (str[i] != '\0')
			print_unicode(str[i++], len);
		unicode_strdel(&str);
	}
}

int					unicode_simblen(wchar_t c)
{
	int				len;

	len = 0;
	if (c < 0x80)
		len = 1;
	else if (c < 0x800)
		len = 2;
	else if (c < 0x10000)
		len = 3;
	else if (c < 0x110000)
		len = 4;
	return (len);
}

int					unicode_strlen(wchar_t *str)
{
	int				i;
	int				len;

	i = 0;
	len = 0;
	while (str[i])
		len += unicode_simblen(str[i++]);
	return (len);
}
