/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   csp.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstroman <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 17:04:11 by tstroman          #+#    #+#             */
/*   Updated: 2019/10/24 16:17:51 by tstroman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void				print_spec_p(t_spec spec, va_list ap, int *len)
{
	char			*str;

	str = va_arg(ap, char *);
	spec.prec = 0;
	str = ft_itoa_base_ull((unsigned long long)str, 16, 'x');
	spec.spec = 'x';
	prec_zero(&str, spec.prec);
	set_pref(&str, spec.spec);
	if (spec.flag_zero && !spec.flag_minus && !spec.prec)
		if ((int)ft_strlen(str) < spec.width)
			add_zero(&str, spec);
	alignment(spec, str, len);
}

void				print_s(t_spec spec, va_list ap, int *len)
{
	char			*str;

	if (!(str = va_arg(ap, char*)))
		str = ft_strdup("(null)");
	else
		str = ft_strdup(str);
	if (spec.prec > 0 && spec.prec <= (int)ft_strlen(str))
		str[spec.prec] = '\0';
	if (spec.prec == -1)
		str[0] = '\0';
	alignment(spec, str, len);
}

void				print_c(t_spec spec, va_list ap, int *len)
{
	int				c;
	int				len1;

	len1 = 1;
	if (spec.spec == '%')
		c = '%';
	else
		c = va_arg(ap, int);
	if (!spec.flag_minus && spec.width)
		while (len1++ < spec.width)
		{
			if (c == '%' && spec.flag_zero)
				ft_putchar('0', len);
			else
				ft_putchar(' ', len);
		}
	ft_putchar(c, len);
	if (spec.flag_minus)
		while (len1++ < spec.width)
			ft_putchar(' ', len);
}

void				print_spec_c(t_spec spec, va_list ap, int *len)
{
	if ((spec.spec == '%') || (spec.length != 1 && spec.spec == 'c'))
		print_c(spec, ap, len);
	if (spec.spec == 'C' || (spec.spec == 'c' && spec.length == 1))
		print_spec_l_c(spec, ap, len);
}

void				print_spec_s(t_spec spec, va_list ap, int *len)
{
	if (spec.spec == 's' && spec.length != 1)
		print_s(spec, ap, len);
	if ((spec.spec == 's' && spec.length == 1) || spec.spec == 'S')
		print_spec_l_s(spec, ap, len);
}
