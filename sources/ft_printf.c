/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstroman <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 17:23:46 by tstroman          #+#    #+#             */
/*   Updated: 2019/10/24 16:19:29 by tstroman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			if_present(const char *str, const char *sub)
{
	size_t		n;

	n = ft_strlen((char*)sub);
	if (!ft_memcmp(str, sub, n))
		return (n);
	return (0);
}

int			scan_color(char *format)
{
	int			i;

	i = 0;
	if ((i += if_present(format, "{RED}")))
		ft_printf(RED);
	else if ((i += if_present(format, "{BLACK}")))
		ft_printf(BLACK);
	else if ((i += if_present(format, "{WHITE}")))
		ft_printf(WHITE);
	else if ((i += if_present(format, "{YELLOW}")))
		ft_printf(YELLOW);
	else if ((i += if_present(format, "{PURPUL}")))
		ft_printf(PURPUL);
	else if ((i += if_present(format, "{GREEN}")))
		ft_printf(GREEN);
	else if ((i += if_present(format, "{RED}")))
		ft_printf(RED);
	else if ((i += if_present(format, "{BLUE}")))
		ft_printf(BLUE);
	else if ((i += if_present(format, "{CYAN}")))
		ft_printf(CYAN);
	else if ((i += if_present(format, "{EOC}")))
		ft_printf(EOC);
	return (i);
}

void		perform_spec(t_spec spec, va_list ap, int *len)
{
	if (spec.spec == 'd' || spec.spec == 'D' || spec.spec == 'i')
		print_spec_d_i(spec, length_d_i(spec, ap), len);
	if (spec.spec == 'o' || spec.spec == 'O')
		print_spec_o(spec, length_u_o_x(spec, ap), len);
	if (spec.spec == 'u' || spec.spec == 'U')
		print_spec_u(spec, length_u_o_x(spec, ap), len);
	if (spec.spec == 'x' || spec.spec == 'X')
		print_spec_x(spec, length_u_o_x(spec, ap), len);
	if (spec.spec == 'f' || spec.spec == 'F')
		print_spec_f(spec, length_f(spec, ap), len);
	if (spec.spec == 's' || spec.spec == 'S')
		print_spec_s(spec, ap, len);
	if (spec.spec == 'c' || spec.spec == 'C' || spec.spec == '%')
		print_spec_c(spec, ap, len);
	if (spec.spec == 'p')
		print_spec_p(spec, ap, len);
	if (spec.spec == 'b')
		print_spec_b(spec, length_u_o_x(spec, ap), len);
}

int			scan_spec(char *format, va_list ap, int *len)
{
	int			i;
	t_spec		spec;

	i = 0;
	scan_flag(&spec, format);
	spec.width = scan_width(format, ap);
	spec.prec = scan_prec(format, ap);
	spec.length = scan_length(format);
	while (format[i] && !is_spec(format[i]) && is_correct(format[i]))
		i++;
	if (!is_spec(format[i]))
		return (0);
	spec.spec = format[i];
	perform_spec(spec, ap, len);
	return (i + 1);
}

int			ft_printf(char *format, ...)
{
	int			i;
	va_list		ap;
	int			len;

	i = 0;
	len = 0;
	va_start(ap, format);
	while (format[i])
	{
		if (format[i] == '{')
			i += scan_color(&format[i]);
		if (format[i] == '%')
			i += scan_spec(&format[i + 1], ap, &len);
		else
			ft_putchar(format[i], &len);
		i++;
	}
	return (len);
}
