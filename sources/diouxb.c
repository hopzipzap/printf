/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   diouxb.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstroman <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 17:09:34 by tstroman          #+#    #+#             */
/*   Updated: 2019/10/24 16:18:02 by tstroman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		print_spec_d_i(t_spec spec, long long int num, int *len)
{
	char		*str;

	if (num == 0 && spec.prec == -1)
		str = ft_strnew(0);
	else
		str = ft_itoa_ll(num);
	prec_zero(&str, spec.prec);
	if (spec.flag_space || spec.flag_plus)
		set_plus(&str, spec);
	if (!spec.flag_minus && !spec.prec && spec.flag_zero)
		if ((int)ft_strlen(str) < spec.width)
			add_zero(&str, spec);
	alignment(spec, str, len);
}

void		print_spec_o(t_spec spec, unsigned long long num, int *len)
{
	char		*str;

	if (!num && !spec.flag_hash && spec.prec == -1)
		str = ft_strnew(0);
	else
		str = ft_itoa_base_ull(num, 8, spec.spec);
	prec_zero(&str, spec.prec);
	if (num && spec.flag_hash)
		set_pref(&str, spec.spec);
	if (!spec.flag_minus && spec.flag_zero && !spec.prec)
		if ((int)ft_strlen(str) < spec.width)
			add_zero(&str, spec);
	alignment(spec, str, len);
}

void		print_spec_u(t_spec spec, unsigned long long num, int *len)
{
	char		*str;

	if (!num && spec.prec == -1)
		str = ft_strnew(0);
	else
		str = ft_itoa_ull(num);
	prec_zero(&str, spec.prec);
	if (spec.flag_plus || spec.flag_space)
		set_plus(&str, spec);
	if (spec.flag_zero && !spec.flag_minus && !spec.prec)
		if ((int)ft_strlen(str) < spec.width)
			add_zero(&str, spec);
	alignment(spec, str, len);
}

void		print_spec_x(t_spec spec, unsigned long long num, int *len)
{
	char		*str;

	if (!num && spec.prec == -1)
		str = ft_strnew(0);
	else
		str = ft_itoa_base_ull(num, 16, spec.spec);
	prec_zero(&str, spec.prec);
	if (spec.flag_hash && num)
		set_pref(&str, spec.spec);
	if (spec.flag_zero && !spec.flag_minus && !spec.prec)
		if ((int)ft_strlen(str) < spec.width)
			add_zero(&str, spec);
	alignment(spec, str, len);
}

void		print_spec_b(t_spec spec, unsigned long long num, int *len)
{
	char		*str;

	if (!num && spec.prec == -1)
		str = ft_strnew(0);
	else
		str = ft_itoa_base_ull(num, 2, spec.spec);
	prec_zero(&str, spec.prec);
	if (spec.flag_hash && num)
		set_pref(&str, spec.spec);
	if (spec.flag_zero && !spec.flag_minus && !spec.prec)
		if ((int)ft_strlen(str) < spec.width)
			add_zero(&str, spec);
	alignment(spec, str, len);
}
