/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util_1.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstroman <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 19:07:05 by tstroman          #+#    #+#             */
/*   Updated: 2019/10/24 16:24:13 by tstroman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

unsigned long long	atoi_util(int num, int i)
{
	if (i == 0)
		return (1);
	else
		return (num * atoi_util(num, i - 1));
}

void				alignment(t_spec spec, char *str, int *len)
{
	int				len1;

	len1 = (int)ft_strlen(str);
	if (!spec.flag_minus)
		while (len1++ < spec.width)
			ft_putchar(' ', len);
	ft_putstr(str, len);
	if (spec.flag_minus)
		while (len1++ < spec.width)
			ft_putchar(' ', len);
}

char				*round_to(char *str)
{
	int				i;

	i = ft_strlen(str) - 1;
	if ('5' <= str[i] && str[i] <= '9')
	{
		str[i] = '\0';
		while (--i)
		{
			if (str[i] == '.')
				i--;
			if (str[i] == '9')
				str[i] = '0';
			else if ('0' <= str[i] && str[i] <= '8')
			{
				str[i]++;
				return (str);
			}
		}
	}
	str[i] = '\0';
	return (str);
}

int					ft_nbrlen(long long int num)
{
	int				size;

	size = 1;
	if (num < 0)
	{
		size++;
		num = -num;
	}
	while (num >= 10)
	{
		size++;
		num /= 10;
	}
	return (size);
}

int					ft_nbrlen_u(unsigned long long num)
{
	int				size;

	size = 1;
	while (num >= 10)
	{
		size++;
		num /= 10;
	}
	return (size);
}
