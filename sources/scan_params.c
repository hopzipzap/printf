/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scan_params.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstroman <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 18:23:05 by tstroman          #+#    #+#             */
/*   Updated: 2019/10/23 19:06:49 by tstroman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		scan_flag(t_spec *spec, char *format)
{
	int		i;

	i = 0;
	spec->flag_minus = 0;
	spec->flag_space = 0;
	spec->flag_plus = 0;
	spec->flag_hash = 0;
	spec->flag_zero = 0;
	while (is_flag(format[i]))
	{
		if (format[i] == '-')
			spec->flag_minus = 1;
		if (format[i] == '+')
			spec->flag_plus = 1;
		if (format[i] == ' ')
			spec->flag_space = 1;
		if (format[i] == '0')
			spec->flag_zero = 1;
		if (format[i] == '#')
			spec->flag_hash = 1;
		i++;
	}
}

int			scan_prec(char *format, va_list ap)
{
	int		i;

	i = 0;
	while (format[i] && !IS_ALPHA(format[i]) && format[i] != '.')
		i++;
	if (format[i++] == '.')
	{
		if (format[i] == '*')
			return (va_arg(ap, int));
		if (ft_atoi(&format[i]))
			return (ft_atoi(&format[i]));
		else
			return (-1);
	}
	return (0);
}

int			scan_width(char *format, va_list ap)
{
	int		i;

	i = 0;
	while (is_flag(format[i]))
		i++;
	if (format[i] == '*')
		return (va_arg(ap, int));
	if (ft_atoi(&format[i]))
		return (ft_atoi(&format[i]));
	return (0);
}

static int	length_present(const char *format, const char *sub)
{
	size_t	n;

	n = ft_strlen((char*)sub);
	while (*format && !is_spec(*format))
		if (!ft_memcmp(format++, sub, n))
			return (1);
	return (0);
}

int			scan_length(char *format)
{
	if (length_present(format, "hh"))
		return (20);
	if (length_present(format, "ll"))
		return (10);
	if (length_present(format, "j"))
		return (5);
	if (length_present(format, "z"))
		return (4);
	if (length_present(format, "L"))
		return (3);
	if (length_present(format, "h"))
		return (2);
	if (length_present(format, "l"))
		return (1);
	return (0);
}
