/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   length.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstroman <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 18:18:44 by tstroman          #+#    #+#             */
/*   Updated: 2019/10/23 18:21:30 by tstroman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

long long int			length_d_i(t_spec spec, va_list ap)
{
	if (spec.length == 1 || spec.spec == 'D')
		return (va_arg(ap, long));
	if (spec.length == 2)
		return ((short int)va_arg(ap, int));
	if (spec.length == 4)
		return (va_arg(ap, size_t));
	if (spec.length == 5)
		return (va_arg(ap, long));
	if (spec.length == 10)
		return (va_arg(ap, long long));
	if (spec.length == 20)
		return ((char)va_arg(ap, int));
	return (va_arg(ap, int));
}

long long int			length_u_o_x(t_spec spec, va_list ap)
{
	if (spec.length == 1 || spec.spec == 'O' || spec.spec == 'U')
		return (va_arg(ap, unsigned long int));
	if (spec.length == 2)
		return ((unsigned short int)va_arg(ap, unsigned int));
	if (spec.length == 4)
		return (va_arg(ap, size_t));
	if (spec.length == 5)
		return (va_arg(ap, long));
	if (spec.length == 10)
		return (va_arg(ap, unsigned long long int));
	if (spec.length == 20)
		return ((unsigned char)va_arg(ap, unsigned int));
	return (va_arg(ap, unsigned int));
}

long double				length_f(t_spec spec, va_list ap)
{
	if (spec.spec == 'f' && spec.length == 3)
		return (va_arg(ap, long double));
	return (va_arg(ap, double));
}
