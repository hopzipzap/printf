/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util_2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstroman <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 19:07:54 by tstroman          #+#    #+#             */
/*   Updated: 2019/10/23 19:07:56 by tstroman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					set_pref(char **str, char spec)
{
	int					i;
	char				*new;

	i = 0;
	if (!(**str == '0' && (spec == 'o' || spec == 'O')))
	{
		new = (char*)malloc(sizeof(char) * ft_strlen(*str) + 3);
		new[i++] = '0';
		if (spec == 'x' || spec == 'X')
			new[i++] = spec;
		ft_strcpy(&new[i], *str);
		ft_strdel(str);
		*str = new;
	}
}

void					set_plus(char **str, t_spec spec)
{
	char				*new;

	if (**str != '-' && spec.spec != 'u')
	{
		new = (char*)malloc(sizeof(char) * ft_strlen(*str) + 2);
		if (spec.flag_plus)
			new[0] = '+';
		else if (spec.flag_space)
			new[0] = ' ';
		ft_strcpy(&new[1], *str);
		ft_strdel(str);
		*str = new;
	}
}

void					add_zero(char **str, t_spec spec)
{
	int					i;
	int					st;
	int					len;
	char				*new;

	i = 0;
	len = ft_strlen(*str);
	new = ft_strnew(spec.width);
	if (**str == '-' || **str == '+' || **str == ' ')
		*new = **str;
	if (**str == '0' && (*str)[1] == spec.spec && spec.spec)
		new = ft_strncpy(new, *str, 2);
	while (new[i])
		i++;
	st = i;
	while (len++ < spec.width)
		new[i++] = '0';
	ft_strcpy(&new[i], *str + st);
	ft_strdel(str);
	*str = new;
}

void					prec_zero(char **str, int prec)
{
	int					i;
	int					len;
	char				*new;

	i = 0;
	len = ft_strlen(*str);
	if (**str == '-')
		len--;
	if (prec && prec > len)
	{
		new = (char*)malloc(sizeof(char) * prec + 2);
		if (**str == '-')
		{
			new[i++] = '-';
			ft_strcpy(&new[prec + 1 - len], &(*str)[1]);
		}
		ft_strcpy(&new[prec - len], *str);
		while (len++ < prec)
			new[i++] = '0';
		ft_strdel(str);
		*str = new;
	}
}

int						ft_cat_pro(char **dest, char *src)
{
	char				*ret;
	char				*fresh;
	char				*dst;

	ret = 0;
	dst = *dest;
	if (dst && src && (fresh = ft_strnew(ft_strlen(dst) + ft_strlen(src))))
	{
		ret = fresh;
		while (*dst)
			*fresh++ = (char)*dst++;
		while (*src)
			*fresh++ = (char)*src++;
	}
	else
		return (0);
	ft_strdel(dest);
	*dest = ret;
	return (1);
}
