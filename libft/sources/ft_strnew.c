/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstroman <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 19:19:49 by tstroman          #+#    #+#             */
/*   Updated: 2019/10/23 19:19:53 by tstroman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strnew(size_t size)
{
	char	*area;

	if (size > size + 1)
		return (0);
	if (!(area = (char*)malloc(size + 1)))
		return (0);
	ft_memset(area, (int)'\0', size + 1);
	return (area);
}
