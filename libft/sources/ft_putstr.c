/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstroman <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 19:18:25 by tstroman          #+#    #+#             */
/*   Updated: 2019/10/23 19:18:29 by tstroman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"

void	ft_putstr(char *src, int *len)
{
	int i;

	i = 0;
	if (src)
	{
		while (src[i] != '\0')
			i++;
		write(1, src, i);
		*len += i;
		ft_strdel(&src);
	}
}
