/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstroman <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 19:19:17 by tstroman          #+#    #+#             */
/*   Updated: 2019/10/23 19:19:21 by tstroman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strjoin(char const *s1, char const *s2)
{
	char	*ret;
	char	*fresh;

	ret = 0;
	if (s1 && s2 && (fresh = ft_strnew(ft_strlen(s1) + ft_strlen(s2))))
	{
		ret = fresh;
		while (*s1)
			*fresh++ = (char)*s1++;
		while (*s2)
			*fresh++ = (char)*s2++;
	}
	return (ret);
}
