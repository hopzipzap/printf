/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstroman <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 19:19:05 by tstroman          #+#    #+#             */
/*   Updated: 2019/10/23 19:19:10 by tstroman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strdup(const char *s1)
{
	int		len;
	int		copycount;
	char	*rez;

	len = 0;
	copycount = 0;
	while (s1[len] != '\0')
		len++;
	rez = (char*)malloc(len + 1);
	if (rez == (char*)0)
		return ((char*)0);
	while (s1[copycount])
	{
		rez[copycount] = s1[copycount];
		copycount++;
	}
	rez[copycount] = '\0';
	return (rez);
}

wchar_t		*unicode_strdup(wchar_t *s1)
{
	int		len;
	int		copycount;
	wchar_t	*rez;

	len = 0;
	copycount = 0;
	while (s1[len] != '\0')
		len++;
	if (!(rez = (wchar_t*)malloc(sizeof(wchar_t) * (len + 1))))
		return (0);
	while (s1[copycount])
	{
		rez[copycount] = s1[copycount];
		copycount++;
	}
	rez[copycount] = '\0';
	return (rez);
}
